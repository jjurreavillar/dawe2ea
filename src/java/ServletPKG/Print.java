/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServletPKG;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Juan José Urrea
 */
public class Print {
    static void ErrorPage(HttpServletResponse response, String message) throws IOException
    {
        try (PrintWriter out = response.getWriter()) 
        {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>OPERATION FAILED - Web Applications "+
                    "Deployment - Second Evaluation - Evaluable Activity</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h2>" + "ERROR: Could not perform the requested operation.<br>" +
                        message + "</h2>");
            out.println("<br>");
            out.println("<a href=\"index.jsp\">Return to the index</a>");
            out.println("</body>");
            out.println("</html>");
        }
    }
}
