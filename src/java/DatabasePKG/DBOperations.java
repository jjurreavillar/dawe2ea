/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DatabasePKG;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Juan José Urrea
 */
public class DBOperations 
{    

    public static void Delete(int id) throws Exception
    {    
        DBConnection dbConnection = new DBConnection();
        dbConnection.Connect();
        
        // try-with-resources will automatically close the resource, whether there's an exception or not
        try
        (PreparedStatement ps = dbConnection.getConnection().prepareStatement("DELETE FROM developer WHERE id=?"))
        {
            ps.setInt(1, id);
            
            ps.executeUpdate();
        }
        catch (Exception e) // This block executes after try-with-resources has closed the resource
        {
            if (e instanceof SQLException)
                throw new Exception("There was a problem deleting the developer: " + e.getMessage());
            else
                throw new Exception("Unknown error at Delete (DBOperations.java): " + e.getMessage());
        }
        finally
        {
            dbConnection.Disconnect();
        }
    }
    
    public static Developer Fetch(int id) throws Exception
    {
        DBConnection dbConnection = new DBConnection();
        dbConnection.Connect();
        
        Developer developer = null;
        
        try
        (PreparedStatement ps = dbConnection.getConnection().prepareStatement("SELECT * FROM developer WHERE id=" + id))
        {
            ResultSet rs = ps.executeQuery();
            
            while (rs.next())
            {
                developer = new Developer();
                developer.setId(rs.getInt("id"));
                developer.setName(rs.getString("name"));
                developer.setSalary(rs.getDouble("salary"));
                developer.setPhone(rs.getInt("phone"));
            }
        }
        catch (Exception e)
        {
            if (e instanceof SQLException)
                throw new Exception("There was a problem fetching the developer: " + e.getMessage());
            else
                throw new Exception("Unknown error at Fetch (DBOperations.java): " + e.getMessage());
        }
        finally
        {
            dbConnection.Disconnect();
        }
        
        return developer;
    }
    
    public static void Insert(Developer developer) throws Exception
    {
        DBConnection dbConnection = new DBConnection();
        dbConnection.Connect();
        
        try
        (PreparedStatement ps = dbConnection.getConnection().prepareStatement("INSERT INTO developer(id, name, salary, phone) VALUES(?, ?, ?, ?)"))
        {            
            ps.setInt(1, developer.getId());
            ps.setString(2, developer.getName());
            ps.setDouble(3, developer.getSalary());
            ps.setInt(4, developer.getPhone());

            ps.executeUpdate();
        }
        catch (Exception e)
        {
            if (e instanceof SQLException)
                throw new Exception("There was a problem inserting the developer: " + e.getMessage());
            else
                throw new Exception("Unknown error at Insert (DBOperations.java): " + e.getMessage());
        }
        finally
        {
            dbConnection.Disconnect();
        }
    }
    
    public static void Update(Developer developer) throws Exception
    {
        DBConnection dbConnection = new DBConnection();
        dbConnection.Connect();
        
        try
        (PreparedStatement ps = dbConnection.getConnection().prepareStatement("UPDATE developer SET name=?, salary=?, phone=? WHERE id=?"))
        {            
            ps.setString(1, developer.getName());
            ps.setDouble(2, developer.getSalary());
            ps.setInt(3, developer.getPhone());
            ps.setInt(4, developer.getId());
            
            ps.executeUpdate();
        }
        catch (Exception e)
        {
            if (e instanceof SQLException)
                throw new Exception("There was a problem updating the developer: " + e.getMessage());
            else
                throw new Exception("Unknown error at Update (DBOperations.java): " + e.getMessage());
        }
        finally
        {
            dbConnection.Disconnect();
        }
    }
}
