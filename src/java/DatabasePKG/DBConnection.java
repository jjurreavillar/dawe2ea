/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DatabasePKG;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author Juan José Urrea
 */
public class DBConnection 
{
    private Connection connection;
    
    /**
     * @return the connection
     */
    public Connection getConnection() {
        return connection;
    }
    
    public void Connect() throws Exception
    {
        if (CreateDB())
        {
            try
            {
                // If the database was created successfully, connect to it
                Context context = new InitialContext();
                DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/Developer");
                connection = ds.getConnection();
            }
            catch (Exception e) // Catch them all and filter them out later
            {
                if (e instanceof SQLException || e instanceof NamingException) // Known
                    throw new Exception("There was an error establishing the connection to the database: " + e.getMessage());
                else // Unknown, rethrow
                    throw new Exception("Unknown error at Connect (DBConnection.java): " + e.getMessage());
            }
            
            CreateTable();
        }
    }
    
    private boolean CreateDB() throws Exception
    {        
        try
        {
            // This connection will attempt to create the database
            Context context = new InitialContext();
            DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/DeveloperCreate");
            Connection conn = ds.getConnection();
            
            PreparedStatement ps = conn.prepareStatement("CREATE DATABASE IF NOT EXISTS dawea_jjurrea");
            ps.executeUpdate();
        }
        catch (Exception e)
        {
            if (e instanceof SQLException || e instanceof NamingException)
                throw new Exception("There was a problem creating the database: " + e.getMessage());
            else
                throw new Exception("Unknown error at Create (DBOperations.java): " + e.getMessage());
        }
                
        return true;
    }
    
    private void CreateTable() throws Exception
    {
        // And now, let's create the table we'll be using...
        try
        (PreparedStatement ps = connection.prepareStatement("CREATE TABLE IF NOT EXISTS developer (" +
                "id INTEGER NOT NULL AUTO_INCREMENT," +
                "name VARCHAR(25) NOT NULL," +
                "salary DOUBLE NOT NULL," +
                "phone INT," +
                "PRIMARY KEY (id)" +
                ")"))
        {
            ps.executeUpdate();
        }
        catch (Exception e)
        {
            if (e instanceof SQLException)
                throw new Exception("There was a problem creating the database: " + e.getMessage());
            else
                throw new Exception("Unknown error at Create (DBOperations.java): " + e.getMessage());
        }
    }
        
    public void Disconnect() throws Exception
    {
        try
        {
            if (connection != null)
                connection.close(); // Close the connection
        }
        catch (Exception e)
        {
            if (e instanceof SQLException)
                throw new Exception("An error occurred when attempting to close the connection to the database: " + e.getMessage());
            else
                throw new Exception("Unknown error at Disconnect (DBConnection.java): " + e.getMessage());
        }
    }
}
