<%-- 
    Document   : bean_db
    Created on : 28-dic-2016, 15:18:55
    Author     : Juan José Urrea
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="dev" scope="request" class="DatabasePKG.Developer" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Show Developer Information (BEAN, DB) - Web Applications Deployment - Second Evaluation - Evaluable Activity</title>
    </head>
    <body>
        <% 
            if (dev.getId() == 0 || dev.getName() == null)
            {
        %>
        <h2>Developer information could not be found for the ID <%= request.getParameter("id") %>.</h2>
        <%
            }
            else
            {
        %>
        <h1>Developer information:</h1>
        <p>Id: <jsp:getProperty name="dev" property="id"/></p>
        <p>Name: <jsp:getProperty name="dev" property="name"/></p>
        <p>Salary: <jsp:getProperty name="dev" property="salary"/></p>
        <p>Phone: <jsp:getProperty name="dev" property="phone"/></p>
        <%
            }
        %>
        <br>
        <a href="index.jsp">Return to the index</a>
    </body>
</html>
