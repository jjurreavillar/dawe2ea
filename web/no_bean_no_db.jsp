<%-- 
    Document   : no_bean_no_db
    Created on : 28-dic-2016, 15:19:05
    Author     : Juan José Urrea
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Show Developer Information (NO BEAN, NO DB) - Web Applications Deployment - Second Evaluation - Evaluable Activity</title>
    </head>
    <body>
        <h1>Developer information:</h1>
        <%
            String id = request.getParameter("id");
            if (id.matches("\\d+"))
            {
        %>
        <p>ID: <%= id %></p>
        <%
            }
            else
            {
        %>
        <p>Invalid ID.<p>
        <%
            }

            String name = request.getParameter("name");
            if (name != null && !name.isEmpty())
            {
        %>
        <p>Name: <%= name %></p>
        <%
            }

            String salary = request.getParameter("salary");
            if (salary.matches("\\d+[.,]?[\\d]{1,2}?"))
            {
        %>
        <p>Salary: <%= salary %></p>
        <%
            }
            else
            {
        %>
        <p>Invalid salary.<p>
        <%
            }
            
            String phone = request.getParameter("phone");
            if (phone.matches("\\d+"))
            {
        %>
        <p>Phone: <%= phone %></p>
        <%
            }
        %>
        <a href="index.jsp">Return to the index</a>
    </body>
</html>
