<!DOCTYPE html>
<html>
    <head>
        <title>Web Applications Deployment - Second Evaluation - Evaluable Activity</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1>Perform an operation:</h1>
        <h3>Insert a developer into the database</h3>
        <form action="Insert" method="post">
            <label>ID: </label>
            <input type="text" name="id">
            <br>
            <label>Name: </label>
            <input type="text" name="name">
            <br>
            <label>Salary: </label>
            <input type="text" name="salary">
            <br>
            <label>Phone: </label>
            <input type="text" name="phone">
            <br>
            <input type="submit">
        </form>
        <h3>Fetch developer information from the database (bean, database)</h3>
        <form action="Fetch" method="post">
            <label>ID: </label>
            <input type="text" name="id">
            <br>
            <input type="submit">
        </form>
        <h3>Show developer information from the form (no bean, no database)</h3>
        <form action="no_bean_no_db.jsp" method="post">
            <label>ID: </label>
            <input type="text" name="id">
            <br>
            <label>Name: </label>
            <input type="text" name="name">
            <br>
            <label>Salary: </label>
            <input type="text" name="salary">
            <br>
            <label>Phone: </label>
            <input type="text" name="phone">
            <br>
            <input type="submit">
        </form>
        <h3>Remove a developer from the database</h3>
        <form action="Delete" method="post">
            <label>ID: </label>
            <input type="text" name="id">
            <br>
            <input type="submit">
        </form>
        <h3>Update developer information</h3>
        <form action="Update" method="post">
            <label>ID: </label>
            <input type="text" name="id">
            <br>
            <label>Name: </label>
            <input type="text" name="name">
            <br>
            <label>Salary: </label>
            <input type="text" name="salary">
            <br>
            <label>Phone: </label>
            <input type="text" name="phone">
            <br>
            <input type="submit">
        </form>
        
        <br>
        <a href="manual.pdf">User manual</a>
    </body>
</html>
